# full-stack-todo-rust-course - Code Samples

- [Course Repository](https://github.com/brooks-builds/full-stack-todo-rust-course)
- [Playlist](https://www.youtube.com/playlist?list=PLrmY5pVcnuE-_CP7XZ_44HN-mDrLQV4nS)
  by [Brooks Patton](https://twitter.com/Brooks_Patton)
