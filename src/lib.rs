mod database;
mod routes;
mod utils;

use std::env;

use axum::extract::FromRef;
use routes::create_routes;
use sea_orm::{Database, DatabaseConnection};
use utils::jwt::TokenSecret;

#[derive(Clone, FromRef)]
pub struct ServerState {
    db: DatabaseConnection,
    token_secret: TokenSecret,
}

pub async fn run() {
    let token_secret = env::var("TOKEN_SECRET").unwrap_or_default().into();

    let database_url = env::var("DATABASE_URL").unwrap_or_default();
    let db = match Database::connect(database_url).await {
        Ok(db) => db,
        Err(err) => {
            eprintln!("Error connecting to database:\n\t{:?}", err);

            panic!()
        }
    };

    let app = create_routes(ServerState { db, token_secret }).await;

    axum::Server::bind(&"127.0.0.1:5000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
