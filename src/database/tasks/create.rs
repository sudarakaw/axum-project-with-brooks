use axum::http::StatusCode;
use sea_orm::{ActiveModelTrait, DatabaseConnection, Set};

use crate::{
    database::{orm::tasks, users},
    routes::api::{self, v1::tasks::RequestTask},
};

use super::{get, task_model_result};

pub async fn create(
    db: &DatabaseConnection,
    user: &users::Model,
    task: RequestTask,
) -> Result<tasks::Model, api::Error> {
    let new_task = tasks::ActiveModel {
        title: Set(task.title),
        description: Set(task.description),
        priority: Set(task.priority),
        user_id: Set(Some(user.id)),
        ..Default::default()
    };

    let saved_task = new_task.save(db).await.map_err(|err| {
        eprintln!("Failed to create task:\n\t{:?}", err);

        api::error(
            StatusCode::INTERNAL_SERVER_ERROR,
            "failed to create new task",
        )
    })?;

    task_model_result(saved_task)
}

pub async fn create_defaults(
    db: &DatabaseConnection,
    user: &users::Model,
) -> Result<(), api::Error> {
    let default_tasks = get::defaults(db).await?;

    for task in default_tasks {
        let new_task = tasks::ActiveModel {
            title: Set(task.title),
            description: Set(task.description),
            priority: Set(task.priority),
            completed_at: Set(task.completed_at),
            deleted_at: Set(task.deleted_at),
            user_id: Set(Some(user.id)),
            ..Default::default()
        };

        new_task.save(db).await.map_err(|err| {
            eprintln!("Failed to create default tasks:\n\t{:?}", err);

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to create default tasks",
            )
        })?;
    }

    Ok(())
}
