use axum::http::StatusCode;
use sea_orm::{
    prelude::DateTimeWithTimeZone, ActiveModelTrait, ColumnTrait, DatabaseConnection, EntityTrait,
    IntoActiveModel, QueryFilter, Set,
};
use serde::Deserialize;

use crate::{
    database::orm::{prelude::Tasks, tasks},
    routes::api,
};

#[derive(Default, Deserialize)]
pub struct PartialTask {
    pub title: Option<String>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub description: Option<Option<String>>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub priority: Option<Option<String>>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub completed_at: Option<Option<DateTimeWithTimeZone>>,

    #[serde(
        default,
        skip_serializing_if = "Option::is_none",
        with = "::serde_with::rust::double_option"
    )]
    pub deleted_at: Option<Option<DateTimeWithTimeZone>>,
}

pub async fn update(
    db: &DatabaseConnection,
    user_id: i32,
    task_id: i32,
    partial_task: PartialTask,
) -> Result<(), api::Error> {
    let result = Tasks::find_by_id(task_id)
        .filter(tasks::Column::UserId.eq(user_id))
        .filter(tasks::Column::DeletedAt.is_null())
        .one(db)
        .await
        .map_err(|err| {
            eprintln!("Failed to get tasks from database:\n\t{:?}", err);

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to get tasks from database.",
            )
        })?;

    if let Some(task) = result {
        let mut task = task.into_active_model();

        if let Some(title) = partial_task.title {
            task.title = Set(title);
        }

        if let Some(description) = partial_task.description {
            task.description = Set(description);
        }

        if let Some(priority) = partial_task.priority {
            task.priority = Set(priority);
        }

        if let Some(completed_at) = partial_task.completed_at {
            task.completed_at = Set(completed_at);
        }

        if let Some(deleted_at) = partial_task.deleted_at {
            task.deleted_at = Set(deleted_at);
        }

        task.save(db).await.map_err(|err| {
            eprintln!(
                "Failed to update task completed_at in database:\n\t{:?}",
                err
            );

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to update task in database.",
            )
        })?;

        Ok(())
    } else {
        api::error_result(StatusCode::NOT_FOUND, "not found")
    }
}
