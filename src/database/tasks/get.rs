use axum::http::StatusCode;
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter};

use crate::{
    database::orm::{prelude::Tasks, tasks, users},
    routes::api,
};

pub async fn defaults(db: &DatabaseConnection) -> Result<Vec<tasks::Model>, api::Error> {
    Tasks::find()
        .filter(tasks::Column::IsDefault.eq(true))
        .filter(tasks::Column::UserId.is_null())
        .filter(tasks::Column::DeletedAt.is_null())
        .all(db)
        .await
        .map_err(|err| {
            eprintln!("Failed to get default tasks:\n\t{:?}", err);

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to get default tasks",
            )
        })
}

pub async fn all(
    db: &DatabaseConnection,
    user: &users::Model,
) -> Result<Vec<tasks::Model>, api::Error> {
    Tasks::find()
        .filter(tasks::Column::UserId.eq(user.id))
        .filter(tasks::Column::DeletedAt.is_null())
        .all(db)
        .await
        .map_err(|err| {
            eprintln!("Failed to get tasks from database:\n\t{:?}", err);

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to get tasks from database.",
            )
        })
}

pub async fn one(
    db: &DatabaseConnection,
    user: &users::Model,
    task_id: i32,
) -> Result<Option<tasks::Model>, api::Error> {
    Tasks::find_by_id(task_id)
        .filter(tasks::Column::UserId.eq(user.id))
        .filter(tasks::Column::DeletedAt.is_null())
        .one(db)
        .await
        .map_err(|err| {
            eprintln!("Failed to get tasks from database:\n\t{:?}", err);

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Failed to get tasks from database.",
            )
        })
}
