pub mod get;

mod create;
mod update;

pub use create::*;
pub use update::*;

use axum::http::StatusCode;
use sea_orm::TryIntoModel;

use crate::routes::api;

use super::orm::tasks;

fn task_model_result(active_model: tasks::ActiveModel) -> Result<tasks::Model, api::Error> {
    active_model.try_into_model().map_err(|err| {
        eprintln!("Failed to get created task model:\n\t{:?}", err);

        api::error(
            StatusCode::INTERNAL_SERVER_ERROR,
            "failed to create new task",
        )
    })
}
