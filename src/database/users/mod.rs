mod create;
pub mod get;

pub use create::create;

use axum::http::StatusCode;
use sea_orm::TryIntoModel;

use crate::routes::api;

use super::orm::users;

pub use super::orm::users::Model;

fn user_model_result(active_model: users::ActiveModel) -> Result<users::Model, api::Error> {
    active_model.try_into_model().map_err(|err| {
        eprintln!("Failed to get created user model:\n\t{:?}", err);

        api::error(
            StatusCode::INTERNAL_SERVER_ERROR,
            "failed to create new user",
        )
    })
}
