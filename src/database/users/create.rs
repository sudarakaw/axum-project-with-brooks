use axum::http::StatusCode;
use sea_orm::{ActiveModelTrait, DatabaseConnection, Set};

use crate::{
    database::orm::users,
    routes::api::{self, v1::users::RequestUser},
    utils::password,
};

use super::user_model_result;

pub async fn create(
    db: &DatabaseConnection,
    token: String,
    user: RequestUser,
) -> Result<users::Model, api::Error> {
    let new_user = users::ActiveModel {
        username: Set(user.username),
        password: Set(password::hash(&user.password)?),
        token: Set(Some(token)),
        ..Default::default()
    }
    .save(db)
    .await
    .map_err(|err| {
        let error_message = err.to_string();

        if error_message
            .contains("duplicate key value violates unique constraint \"users_username_key\"")
        {
            api::error(
                StatusCode::BAD_REQUEST,
                "Username already taken, try again with a different user name",
            )
        } else {
            eprintln!("Error creating user:\n\t{:?}", err);

            api::error(
                StatusCode::INTERNAL_SERVER_ERROR,
                "Something went wrong, please try again.",
            )
        }
    })?;

    user_model_result(new_user)
}
