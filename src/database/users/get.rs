use axum::http::StatusCode;
use sea_orm::{ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter};

use crate::{
    database::orm::{prelude::Users, users},
    routes::api,
};

pub async fn by_token(
    db: &DatabaseConnection,
    token: &str,
) -> Result<Option<users::Model>, api::Error> {
    Users::find()
        .filter(users::Column::Token.eq(Some(token)))
        .one(db)
        .await
        .map_err(|err| {
            eprintln!("Token does not exist in database:\n\t{:?}", err);

            api::error(StatusCode::INTERNAL_SERVER_ERROR, "")
        })
}

pub async fn by_username(
    db: &DatabaseConnection,
    username: &str,
) -> Result<Option<users::Model>, api::Error> {
    Users::find()
        .filter(users::Column::Username.eq(username))
        .one(db)
        .await
        .map_err(|err| {
            eprintln!("User does not exist in database:\n\t{:?}", err);

            api::error(StatusCode::INTERNAL_SERVER_ERROR, "")
        })
}
