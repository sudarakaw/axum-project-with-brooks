use axum::http::StatusCode;

use crate::routes::api;

const BCRYPT_COST: u32 = 12;

pub fn hash(plain_text_password: &str) -> Result<String, api::Error> {
    bcrypt::hash(plain_text_password, BCRYPT_COST)
        .map_err(|_| api::error(StatusCode::INTERNAL_SERVER_ERROR, "Failed to hash password"))
}

pub fn verify(password: &str, hash: &str) -> Result<bool, api::Error> {
    bcrypt::verify(password, hash).map_err(|_| {
        api::error(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Failed to verify password",
        )
    })
}
