use axum::http::StatusCode;
use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

use crate::routes::api;

#[derive(Clone)]
pub struct TokenSecret(String);

impl From<String> for TokenSecret {
    fn from(value: String) -> Self {
        Self(value)
    }
}

#[derive(Serialize, Deserialize)]
struct Claims {
    exp: usize,
    iat: usize,
    username: String,
}

pub fn create(secret: &TokenSecret, username: String) -> Result<String, api::Error> {
    let mut now = Utc::now();
    let iat = now.timestamp() as usize;

    now += Duration::hours(1);
    let exp = now.timestamp() as usize;

    let claims = Claims { exp, iat, username };

    encode(
        &Header::default(),
        &claims,
        &EncodingKey::from_secret(secret.0.as_bytes()),
    )
    .map_err(|_| {
        api::error(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Failed to generate access token",
        )
    })
}

pub fn is_valid(secret: &TokenSecret, token: &str) -> Result<(), api::Error> {
    decode::<Claims>(
        token,
        &DecodingKey::from_secret(secret.0.as_bytes()),
        &Validation::default(),
    )
    .map_err(|err| match err.kind() {
        jsonwebtoken::errors::ErrorKind::InvalidToken => {
            api::error(StatusCode::UNAUTHORIZED, "not authenticated!")
        }
        jsonwebtoken::errors::ErrorKind::ExpiredSignature => api::error(
            StatusCode::UNAUTHORIZED,
            "Your session has expired, please login again",
        ),
        _ => {
            eprintln!("Error validating token:\n\t{:?}", err);

            api::error(StatusCode::INTERNAL_SERVER_ERROR, "Error validating token")
        }
    })?;

    Ok(())
}
