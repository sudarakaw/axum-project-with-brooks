use axum_project_with_brooks::run;
use dotenvy::dotenv;

#[tokio::main]
async fn main() {
    dotenv().ok();

    run().await
}
