use axum::{http::StatusCode, response::IntoResponse, Json};
use serde::Serialize;

#[derive(Serialize)]
pub struct Data<T> {
    #[serde(skip)]
    code: StatusCode,

    data: T,
}

impl<T> IntoResponse for Data<T>
where
    T: Serialize,
{
    fn into_response(self) -> axum::response::Response {
        (self.code, Json(self)).into_response()
    }
}

pub fn ok<T, E>(data: T) -> Result<Data<T>, E> {
    Ok(Data {
        code: StatusCode::OK,
        data,
    })
}
pub fn created<T, E>(data: T) -> Result<Data<T>, E> {
    Ok(Data {
        code: StatusCode::CREATED,
        data,
    })
}
