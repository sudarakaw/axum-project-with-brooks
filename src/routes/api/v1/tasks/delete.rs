use axum::{
    extract::{Path, State},
    Extension,
};
use chrono::Utc;
use sea_orm::DatabaseConnection;

use crate::{
    database::{
        tasks::{self, PartialTask},
        users,
    },
    routes::api,
};

pub async fn soft_delete(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    Path(task_id): Path<i32>,
) -> Result<api::Data<()>, api::Error> {
    let now = Utc::now();

    let task = PartialTask {
        deleted_at: Some(Some(now.into())),
        ..Default::default()
    };

    tasks::update(&db, user.id, task_id, task).await?;

    api::ok(())
}
