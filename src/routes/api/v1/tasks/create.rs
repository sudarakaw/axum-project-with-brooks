use axum::{
    async_trait,
    body::HttpBody,
    extract::{FromRequest, State},
    http::StatusCode,
    BoxError, Extension, Json, RequestExt,
};
use sea_orm::DatabaseConnection;
use serde::Deserialize;

use crate::{
    database::{tasks, users},
    routes::api,
};

use super::Task;

#[derive(Deserialize)]
pub struct RequestTask {
    pub title: String,
    pub description: Option<String>,
    pub priority: Option<String>,
}

#[async_trait]
impl<S, B> FromRequest<S, B> for RequestTask
where
    B: Send + HttpBody + 'static,
    B::Data: Send,
    B::Error: Into<BoxError>,
    S: Send + Sync,
{
    type Rejection = api::Error;

    async fn from_request(
        req: axum::http::Request<B>,
        _state: &S,
    ) -> Result<Self, Self::Rejection> {
        let Json(task) = req.extract::<Json<RequestTask>, _>().await.map_err(|err| {
            if err.body_text().contains("missing field `title`") {
                api::error(StatusCode::BAD_REQUEST, "missing task title")
            } else {
                eprintln!(
                    "Error getting task information in custom create task extractor:\n\t{:?}",
                    err
                );

                api::error(StatusCode::INTERNAL_SERVER_ERROR, "Internal server error")
            }
        })?;

        if task.title.is_empty() {
            return api::error_result(StatusCode::BAD_REQUEST, "missing task title");
        }

        Ok(task)
    }
}

pub async fn create(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    request_task: RequestTask,
) -> Result<api::Data<Task>, api::Error> {
    let saved_task = tasks::create(&db, &user, request_task).await?;

    api::created(Task {
        id: saved_task.id,
        title: saved_task.title,
        description: saved_task.description,
        priority: saved_task.priority,
        completed_at: saved_task.completed_at,
    })
}
