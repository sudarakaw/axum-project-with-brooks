use axum::{
    extract::{Path, State},
    Extension, Json,
};
use chrono::Utc;
use sea_orm::DatabaseConnection;

use crate::{
    database::{
        tasks::{self, PartialTask},
        users,
    },
    routes::api,
};

pub async fn complete(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    Path(task_id): Path<i32>,
) -> Result<api::Data<()>, api::Error> {
    let now = Utc::now();

    let task = PartialTask {
        completed_at: Some(Some(now.into())),
        ..Default::default()
    };

    tasks::update(&db, user.id, task_id, task).await?;

    api::ok(())
}

pub async fn uncomplete(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    Path(task_id): Path<i32>,
) -> Result<api::Data<()>, api::Error> {
    let task = PartialTask {
        completed_at: Some(None),
        ..Default::default()
    };

    tasks::update(&db, user.id, task_id, task).await?;

    api::ok(())
}

pub async fn partial_update(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    Path(task_id): Path<i32>,
    Json(partial_task): Json<PartialTask>,
) -> Result<api::Data<()>, api::Error> {
    tasks::update(&db, user.id, task_id, partial_task).await?;

    api::ok(())
}
