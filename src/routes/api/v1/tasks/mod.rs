mod create;
mod delete;
mod get;
mod update;

pub use create::{create, RequestTask};
pub use delete::soft_delete;
pub use get::{get_all, get_one};
use sea_orm::prelude::DateTimeWithTimeZone;
use serde::Serialize;
pub use update::{complete, partial_update, uncomplete};

#[derive(Serialize)]
pub struct Task {
    id: i32,
    title: String,
    description: Option<String>,
    priority: Option<String>,
    completed_at: Option<DateTimeWithTimeZone>,
}
