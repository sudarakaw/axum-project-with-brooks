use axum::{
    extract::{Path, State},
    http::StatusCode,
    Extension,
};
use sea_orm::DatabaseConnection;

use crate::{
    database::{tasks, users},
    routes::api,
};

use super::Task;

pub async fn get_all(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
) -> Result<api::Data<Vec<Task>>, api::Error> {
    let tasks = tasks::get::all(&db, &user)
        .await?
        .into_iter()
        .map(|task| Task {
            id: task.id,
            title: task.title,
            description: task.description,
            priority: task.priority,
            completed_at: task.completed_at,
        })
        .collect::<Vec<Task>>();

    api::ok(tasks)
}

pub async fn get_one(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
    Path(id): Path<i32>,
) -> Result<api::Data<Task>, api::Error> {
    let result = tasks::get::one(&db, &user, id).await?;

    if let Some(task) = result {
        api::ok(Task {
            id: task.id,
            title: task.title,
            description: task.description,
            priority: task.priority,
            completed_at: task.completed_at,
        })
    } else {
        api::error_result(StatusCode::NOT_FOUND, "not found")
    }
}
