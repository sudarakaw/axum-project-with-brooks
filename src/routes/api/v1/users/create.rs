use axum::{extract::State, Json};
use sea_orm::DatabaseConnection;
use serde::{Deserialize, Serialize};

use crate::{
    database::{tasks, users},
    routes::api,
    utils::jwt::{self, TokenSecret},
};

#[derive(Serialize)]
pub struct ResponseUser {
    id: i32,
    username: String,
    token: String,
}

#[derive(Deserialize)]
pub struct RequestUser {
    pub username: String,
    pub password: String,
}

pub async fn create(
    State(db): State<DatabaseConnection>,
    State(token_secret): State<TokenSecret>,
    Json(request_user): Json<RequestUser>,
) -> Result<api::Data<ResponseUser>, api::Error> {
    let token = jwt::create(&token_secret, request_user.username.clone())?;
    let new_user = users::create(&db, token, request_user).await?;

    tasks::create_defaults(&db, &new_user).await?;

    api::created(ResponseUser {
        id: new_user.id,
        username: new_user.username,
        token: new_user.token.unwrap_or_default(),
    })
}
