use axum::http::StatusCode;
use axum::{extract::State, Json};
use sea_orm::{ActiveModelTrait, DatabaseConnection, IntoActiveModel, Set};
use serde::{Deserialize, Serialize};

use crate::database::users;
use crate::routes::api;

use crate::utils::jwt::{self, TokenSecret};
use crate::utils::password;

#[derive(Deserialize)]
pub struct RequestUser {
    username: String,
    password: String,
}

#[derive(Serialize)]
pub struct ResponseUser {
    id: i32,
    username: String,
    token: String,
}

pub async fn login(
    State(db): State<DatabaseConnection>,
    State(token_secret): State<TokenSecret>,
    Json(request_user): Json<RequestUser>,
) -> Result<api::Data<ResponseUser>, api::Error> {
    let result = users::get::by_username(&db, &request_user.username).await?;

    if let Some(user) = result {
        if !password::verify(&request_user.password, &user.password)? {
            eprintln!("Failed to verify password hash");

            return api::error_result(
                StatusCode::BAD_REQUEST,
                "incorrect username and/or password",
            );
        }

        let new_token = jwt::create(&token_secret, user.username.clone())?;
        let mut user = user.into_active_model();

        user.token = Set(Some(new_token));

        let saved_user = user.save(&db).await.map_err(|err| {
            eprintln!("Error adding token to user in database:\n\t{:?}", err);

            api::error(StatusCode::INTERNAL_SERVER_ERROR, "Error logging you in")
        })?;

        api::ok(ResponseUser {
            id: saved_user.id.unwrap(),
            username: saved_user.username.unwrap(),
            token: saved_user.token.unwrap().unwrap_or_default(),
        })
    } else {
        api::error_result(
            StatusCode::BAD_REQUEST,
            "incorrect username and/or password",
        )
    }
}
