use axum::{extract::State, http::StatusCode, Extension};
use sea_orm::{ActiveModelTrait, DatabaseConnection, IntoActiveModel, Set};

use crate::{database::users, routes::api};

pub async fn logout(
    State(db): State<DatabaseConnection>,
    Extension(user): Extension<users::Model>,
) -> Result<api::Data<()>, api::Error> {
    let mut user = user.into_active_model();

    user.token = Set(None);

    user.save(&db).await.map_err(|err| {
        eprintln!("Failed to logout:\n\t{:?}", err);

        api::error(
            StatusCode::INTERNAL_SERVER_ERROR,
            "Failed to logout, please try again later",
        )
    })?;

    api::ok(())
}
