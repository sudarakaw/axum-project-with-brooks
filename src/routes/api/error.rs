use axum::{http::StatusCode, response::IntoResponse, Json};
use serde::{ser::SerializeStruct, Serialize};

pub struct Error {
    status: StatusCode,
    error: String,
}

impl Serialize for Error {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut s = serializer.serialize_struct("Error", 2)?;

        s.serialize_field("status", &Into::<u16>::into(self.status))?;
        s.serialize_field("error", &self.error)?;

        s.end()
    }
}

impl IntoResponse for Error {
    fn into_response(self) -> axum::response::Response {
        (self.status, Json(self)).into_response()
    }
}

pub fn error(status: StatusCode, message: impl Into<String>) -> Error {
    Error {
        status,
        error: message.into(),
    }
}

pub fn error_result<T>(status: StatusCode, message: impl Into<String>) -> Result<T, Error> {
    Err(error(status, message))
}
