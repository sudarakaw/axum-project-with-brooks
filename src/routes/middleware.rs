use axum::{
    extract::State,
    http::{Request, StatusCode},
    middleware::Next,
    response::Response,
};
use sea_orm::DatabaseConnection;

use crate::{
    database::users,
    utils::jwt::{self, TokenSecret},
};

use super::api;

pub async fn require_authentication<T>(
    State(db): State<DatabaseConnection>,
    State(token_secret): State<TokenSecret>,
    mut request: Request<T>,
    next: Next<T>,
) -> Result<Response, api::Error> {
    let token = if let Some(token) = request.headers().get("x-auth-token") {
        token.to_str().unwrap_or_default()
    } else {
        return api::error_result(StatusCode::UNAUTHORIZED, "not authenticated!");
    };

    let result = users::get::by_token(&db, token).await?;

    jwt::is_valid(&token_secret, token)?;

    if let Some(user) = result {
        request.extensions_mut().insert(user);
    } else {
        return api::error_result(
            StatusCode::UNAUTHORIZED,
            "You are not authorized, please login or create an account",
        );
    };

    Ok(next.run(request).await)
}
