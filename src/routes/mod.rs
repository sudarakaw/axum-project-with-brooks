pub mod api;
mod middleware;

use axum::{
    routing::{delete, get, patch, post, put},
    Router,
};

use crate::ServerState;

use self::{
    api::v1::{tasks, users},
    middleware::require_authentication,
};

pub async fn create_routes(state: ServerState) -> Router {
    Router::new()
        .route("/api/v1/tasks", post(tasks::create))
        .route("/api/v1/tasks", get(tasks::get_all))
        .route("/api/v1/tasks/:id", get(tasks::get_one))
        .route("/api/v1/tasks/:id", patch(tasks::partial_update))
        .route("/api/v1/tasks/:id", delete(tasks::soft_delete))
        .route("/api/v1/tasks/:id/completed", put(tasks::complete))
        .route("/api/v1/tasks/:id/uncompleted", put(tasks::uncomplete))
        .route("/api/v1/users/logout", post(users::logout))
        .layer(axum::middleware::from_fn_with_state(
            state.clone(),
            require_authentication,
        ))
        .route("/api/v1/users", post(users::create))
        .route("/api/v1/users/login", post(users::login))
        .with_state(state)
}
